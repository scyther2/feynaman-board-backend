require("dotenv").config();

const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
const topicRoutes = require("./routes/topic");
const blockRoutes = require("./routes/block");

//DB Connection
mongoose
  .connect(process.env.MONGODB_URL)
  .then(() => {
    console.log("DB Connected");
  })
  .catch((err) => console.log(err));

// MiddleWare
app.use(cors());
app.use(express.json());

//Routes
app.use("/api", userRoutes);
app.use("/api", topicRoutes);
app.use("/api", blockRoutes);
app.get("/", (req, res) => {
  res.send("HomePage");
});

//Server Listening
app.listen(process.env.PORT, () => {
  console.log(`Listening at ${process.env.PORT}`);
});
