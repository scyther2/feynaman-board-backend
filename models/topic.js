const mongoose = require("mongoose");
const schema = mongoose.Schema;
const { ObjectId } = mongoose.Schema;

var topicSchema = new schema({
  name: {
    type: String,
    required: true,
    trim: true,
    maxlength: 30,
  },
  blocks: { type: [{ type: ObjectId, ref: "Block" }], default: undefined },
  percentage : {
    type: Number,
  }
});

module.exports = mongoose.model("Topic", topicSchema);
