const mongoose = require("mongoose");
const schema = mongoose.Schema;
const { ObjectId } = mongoose.Schema;

var userSchema = new schema({
  userName: {
    type: String,
    required: true,
    trim: true,
    maxlength: 30,
    unique: true,
  },
  topics: { type: [{ type: ObjectId, ref: "Topic" }], default: undefined },
});

// userSchema
// 	.virtual("password")
// 	.set(function (password) {
// 		this._password = password;
// 		this.salt = uuidv1();
// 		this.encrypted_password = this.securePassword(password);
// 	})
// 	.get(function () {
// 		return this._password;
// 	});

// userSchema.methods = {
// 	authentication: function (plainPassword) {
// 		return this.securePassword(plainPassword) === this.encrypted_password;
// 	},
// 	securePassword: function (plainPassword) {
// 		if (!plainPassword) return "";
// 		try {
// 			return crypto
// 				.createHmac("sha256", this.salt)
// 				.update(plainPassword)
// 				.digest("hex");
// 		} catch (err) {
// 			return "";
// 		}
// 	},
// };

module.exports = mongoose.model("User", userSchema);
