const mongoose = require("mongoose");
const schema = mongoose.Schema;

var blockSchema = new schema({
  content: {
    type: String,
    required: true,
    trim: true,
  },
  category: {
    type: String,
    default: "NOT CLEAR",
    enum: ["UNDERSTOOD", "SOMEWHAT UNDERSTOOD", "NOT CLEAR", "WHAT RUBBISH"],
  }
});

module.exports = mongoose.model("Block", blockSchema);
