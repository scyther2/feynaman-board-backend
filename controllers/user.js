const User = require("../models/user");

exports.getUserById = (req, res, next, userName) => {
  User.find({ userName: userName }).populate("topics").exec((error, user) => {
    if (user.length === 0) {
      console.log((req.userName = userName));
    }
    req.profile = user;
    next();
  });
};

exports.getUser = (req, res) => {
  if (!req.userName) {
    return res.json({ user: req.profile[0] });
  }
  User.create({ userName: req.userName }, function (error, user) {
    if (error) {
      return res.status(400).json({
        cError: "user not created",
        error,
      });
    }
    return res.json({ user: user });
  });
};

exports.createUser = (req, res) => {
  User.create(req.userName, function (error, userName) {
    if (error) {
      return res.status(400).json({
        cError: "user not created",
        error,
      });
    }
    return res.json(userName);
  });
};
