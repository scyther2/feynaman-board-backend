const Topic = require("../models/topic");
const Block = require("../models/block");
const User = require("../models/user");

exports.createTopic = (req, res) => {
  Topic.create(req.body, function (error, topic) {
    if (error) {
      return res.status(400).json({
        cError: "Topic not created",
        error,
      });
    }
    User.updateOne(
      { filter: { userName: req.body.userName } },
      { $push: { topics: topic._id } },
      (error, user) => {
        if (error) {
          return res.status(400).json({
            cError: "User not updated",
            error,
          });
        }
        //  res.json(user);
      }
    );
    return res.json(topic);
  });
};

exports.updateContent = (req, res) => {
  console.log(req.body.blocks);
  Block.insertMany(req.body.blocks, (error, result) => {
    if (error) {
      return res.status(400).json({
        cError: "Blocks not Created",
        error,
      });
    }
    const blocks = result;
    const newBlocks = blocks.map((block) => block._id);
    Topic.findByIdAndUpdate(
      req.body.topicID,
      { blocks: newBlocks, percentage: req.body.percentage },
      (error, topic) => {
        if (error) {
          return res.status(400).json({
            cError: "Topic not updated",
            error,
          });
        }
        return res.json(topic);
      }
    );
  });

  // Block.updateOne(
  //   { filter: { _id: req.body.order._id } },
  //   { update: { content: req.body.content, category: req.body.category } }
  // );
};
