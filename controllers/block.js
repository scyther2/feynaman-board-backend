const Block = require("../models/block");

exports.createBlock = (req, res) => {
  Block.create(req.body, function (error, block) {
    if (error) {
      return res.status(400).json({
        cError: "Block not created",
        error,
      });
    }
    return res.json(block);
  });
};
