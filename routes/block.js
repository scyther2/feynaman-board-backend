const express = require("express");
const { createBlock } = require("../controllers/block");
const router = express.Router();

// router.param("userID", getUserById);
// router.get("/user/:userID", getUser);
router.post("/block/create",createBlock)

module.exports = router;
