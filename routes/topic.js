const express = require("express");
const { createTopic ,updateContent} = require("../controllers/topic");
const router = express.Router();

// router.param("userID", getUserById);
// router.get("/user/:userID", getUser);
router.post("/topic/create",createTopic)
router.post("/topic/updateContent",updateContent)

module.exports = router;
