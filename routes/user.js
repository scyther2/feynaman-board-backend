const express = require("express");
const router = express.Router();
const { getUserById, getUser, createUser } = require("../controllers/user");

router.param("userID", getUserById);
router.get("/user/:userID", getUser);
router.post("/user/create",createUser)
// router.get("/listAll",listUsers)
// router.put("/user/:userID",isSignedIn,isAuthenticated,updateUser)
// router.get("/user/orders/:userID",isSignedIn,isAuthenticated,userPurchaseList)

module.exports = router;
